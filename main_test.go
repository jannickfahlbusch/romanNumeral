package romanNumeral

import "testing"

var testValueList map[string]int = map[string]int{
	"I":      1,
	"II":     2,
	"IV":     4,
	"V":      5,
	"IX":     9,
	"XLII":   42,
	"XCIX":   99,
	"MMXIII": 2013,
}

func TestConvertFromRomanToArabic(t *testing.T) {
	for roman, expectedResult := range testValueList {
		result := ConvertFromRomanToArabic(roman)

		if result != expectedResult {
			t.Errorf("Expected %s to be %d but got %d instead.", roman, expectedResult, result)
		}
	}

}

func TestConvertFromRomanToArabicAlternate(t *testing.T) {

	for roman, expectedResult := range testValueList {
		result := ConvertFromRomanToArabicAlternate(roman)

		if result != expectedResult {
			t.Errorf("Expected %s to be %d but got %d instead.", roman, expectedResult, result)
		}
	}

}

func benchmarkConvertFromRomanToArabic(iterationCount int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		for roman := range testValueList {
			ConvertFromRomanToArabic(roman)
		}
	}
}

func BenchmarkConvertFromRomanToArabic1(b *testing.B)    { benchmarkConvertFromRomanToArabic(1, b) }
func BenchmarkConvertFromRomanToArabic10(b *testing.B)   { benchmarkConvertFromRomanToArabic(10, b) }
func BenchmarkConvertFromRomanToArabic100(b *testing.B)  { benchmarkConvertFromRomanToArabic(100, b) }
func BenchmarkConvertFromRomanToArabic1000(b *testing.B) { benchmarkConvertFromRomanToArabic(1000, b) }

func benchmarkConvertFromRomanToArabicAlternate(iterationCount int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		for roman := range testValueList {
			ConvertFromRomanToArabicAlternate(roman)
		}
	}
}

func BenchmarkConvertFromRomanToArabicAlternate1(b *testing.B) {
	benchmarkConvertFromRomanToArabicAlternate(1, b)
}
func BenchmarkConvertFromRomanToArabicAlternate10(b *testing.B) {
	benchmarkConvertFromRomanToArabicAlternate(10, b)
}
func BenchmarkConvertFromRomanToArabicAlternate100(b *testing.B) {
	benchmarkConvertFromRomanToArabicAlternate(100, b)
}
func BenchmarkConvertFromRomanToArabicAlternate1000(b *testing.B) {
	benchmarkConvertFromRomanToArabicAlternate(1000, b)
}

package romanNumeral

import (
	"strings"
)

type RomanCharacterValueList map[string]int

// GetValue returns the corresponding value to the given string
func (r RomanCharacterValueList) GetValue(romanNumber string) int {
	return r[romanNumber]
}

// romanCharacterValueList maps the roman number to a arabic number
var romanCharacterValueList RomanCharacterValueList = RomanCharacterValueList{
	"I": 1,
	"V": 5,
	"X": 10,
	"L": 50,
	"C": 100,
	"D": 500,
	"M": 1000,
}

// ConvertFromRomanToArabic converts an roman number to a decimal one
func ConvertFromRomanToArabic(roman string) int {
	var (
		result       int
		previousChar string
	)

	for _, char := range strings.Split(roman, "") {
		value := romanCharacterValueList.GetValue(char)
		previousValue := romanCharacterValueList.GetValue(previousChar)

		// Check, if the previous character has a lower value than the current one
		if previousChar != "" && previousValue < value {
			result -= previousValue
			result += value - previousValue
		} else {
			result += value
		}

		previousChar = char
	}

	return result
}

// Alternative implementation

// ConvertFromRomanToArabic converts an roman number to a decimal one
func ConvertFromRomanToArabicAlternate(roman string) int {
	var result int

	charList := strings.Split(roman, "")

	for index := range charList {
		currentCharValue := romanCharacterValueList.GetValue(charList[index])

		var nextChar string
		if index+1 < len(charList) {
			nextChar = charList[index+1]
		}

		if nextChar != "" && currentCharValue < romanCharacterValueList[nextChar] {
			result -= currentCharValue
		} else {
			result += currentCharValue
		}
	}

	return result
}
